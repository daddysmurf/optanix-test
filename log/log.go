package log

import "fmt"

const (
	DEBUG = 10
	INFO = 20
	WARN = 30
	ERROR = 40
)
var levelTranslation = map[int]string{
	10: "DEBUG",
	20: "INFO",
	30: "WARN",
	40: "ERROR",
}

func logBase(message string, level int) {
	// TODO: add timestamp
	levelString := levelTranslation[level]
	fmt.Printf("[%s] %s\n", levelString, message)
}

func Debug(message string) { logBase(message, DEBUG) }
func Info(message string)  { logBase(message, INFO) }
func Warn(message string)  { logBase(message, WARN) }
func Error(message string) { logBase(message, ERROR) }
func ErrorE(message error) { logBase(message.Error(), ERROR) }
