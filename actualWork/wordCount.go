package actualWork

import (
	"regexp"
	"fmt"
	"errors"
	"strings"
)

func SplitSentences(sentencesString string) (sentences []string, err error) {
	sentenceSplitter, err := regexp.Compile("(\\.\\s)|(\\.$)")
	if err != nil {
		err = errors.New(fmt.Sprintf("Error creating split regex: %s", err.Error()))
		return
	}

	sentencesRaw := sentenceSplitter.Split(sentencesString, -1)

	sentences = []string{}
	for _, s := range sentencesRaw {
		if strings.TrimSpace(s) == "" {
			continue
		}
		sentences = append(sentences, s)
	}
	return
}

func CountWordsInSentences(sentencesString string) (sentenceCounts map[string]int, err error) {
	sentences, err := SplitSentences(sentencesString)
	if err != nil {
		return
	}
	sentenceCounts = map[string]int{}
	boundaryFinder, err := regexp.Compile("\\s")
	if err != nil {
		return
	}

	for _, s := range sentences {
		words := boundaryFinder.Split(s, -1)
		sentenceCounts[s] = len(words)
	}

	return
}