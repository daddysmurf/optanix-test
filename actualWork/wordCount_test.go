package actualWork_test

import (
	"testing"
	"bitbucket.org/daddysmurf/optanix-test/actualWork"
)

var sampleSentencesA string = "This is one sentence. This is one more sentence."

func TestSplitSentences(t *testing.T) {
	result, err := actualWork.SplitSentences(sampleSentencesA)

	expectedCount := 2
	if err != nil {
		t.Fatal(err)
	}
	if len(result) != expectedCount {
		t.Fatal("Expected len(result) == ", expectedCount, ". Got len(result) == ", len(result))
	}
}

func TestCountWordsInSentences(t *testing.T) {
	result, err := actualWork.CountWordsInSentences(sampleSentencesA)

	var expectedWordCounts = map[string]int{
		"This is one sentence" : 4,
		"This is one more sentence": 5,
	}

	if err != nil {
		t.Fatal(err)
	}
	for s,c := range expectedWordCounts {
		actualC, ok := result[s]
		if !ok {
			t.Fatal("Did not find ", s, " in results.")
		}
		if actualC != c {
			t.Fatal("Expected word count for \"", s, "\" is ", c, ". Got ", result[s])
		}
	}
}
