package actualWork_test

import (
	"testing"
	"bitbucket.org/daddysmurf/optanix-test/actualWork"
)

var sampleSentencesB string = "This is one sentence. This is one more sentence."

func TestCountLetters(t *testing.T) {
	result, err := actualWork.CountLetters(sampleSentencesA)

	expectedCount := 10
	if err != nil {
		t.Fatal(err)
	}
	if len(result) != expectedCount {
		t.Fatal("Expected len(result) == ", expectedCount, ". Got len(result) == ", len(result))
	}
}
