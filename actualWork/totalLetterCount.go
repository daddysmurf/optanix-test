package actualWork

import (
	"strings"
	"unicode"
)

func CountLetters(sentencesString string) (letterCounts map[string]int, err error) {
	letterCounts = map[string]int{}

	for _, letterCase := range sentencesString {
		letter := strings.ToUpper(string(letterCase))
		if strings.TrimSpace(letter) == "" || !unicode.In([]rune(letter)[0], unicode.Latin) {
			continue
		}
		count, ok := letterCounts[letter]
		if !ok {
			count = 0
		}
		count++
		letterCounts[letter] = count
	}

	return
}