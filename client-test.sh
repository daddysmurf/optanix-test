#!/usr/bin/env sh

###
### Word Count Per Sentence
###

# invalid method
curl --request GET \
    http://127.0.0.1:8088/word_count_per_sentence
echo

# invalid payload
curl --request POST \
    --data 'This is a sentence. There are many like it, but this one is mine. Without my sentence, I am nothing' \
    http://127.0.0.1:8088/word_count_per_sentence
echo

# valid
curl --request POST \
    --data '{"sentences": "This is a sentence. There are many like it, but this one is mine. Without my sentence, I am nothing"}' \
    http://127.0.0.1:8088/word_count_per_sentence
echo

# empty content
curl --request POST \
    --data '{"sentences": ""}' \
    http://127.0.0.1:8088/word_count_per_sentence
echo

# not enough sentences
curl --request POST \
    --data '{"sentences": "This is a sentence."}' \
    http://127.0.0.1:8088/word_count_per_sentence
echo

###
### Total Letter Count
###

# invalid method
curl --request GET \
    http://127.0.0.1:8088/total_letter_count
echo

# invalid payload
curl --request POST \
    --data 'This is a sentence. There are many like it, but this one is mine. Without my sentence, I am nothing' \
    http://127.0.0.1:8088/total_letter_count
echo

# valid
curl --request POST \
    --data '{"sentences": "This is a sentence. There are many like it, but this one is mine. Without my sentence, I am nothing"}' \
    http://127.0.0.1:8088/total_letter_count
echo

# empty content
curl --request POST \
    --data '{"sentences": ""}' \
    http://127.0.0.1:8088/total_letter_count
echo

# not enough sentences
curl --request POST \
    --data '{"sentences": "This is a sentence."}' \
    http://127.0.0.1:8088/total_letter_count
echo
