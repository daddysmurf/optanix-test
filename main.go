package main

import (
	"bitbucket.org/daddysmurf/optanix-test/log"
	"net/http"
)

func main() {
	http.HandleFunc("/word_count_per_sentence", WordCountPerSentence)
	http.HandleFunc("/total_letter_count", TotalLetterCount)
	log.Info("Starting Server...\n")
	log.ErrorE(http.ListenAndServe(":8088", nil))
}
