package main

import (
	"bitbucket.org/daddysmurf/optanix-test/actualWork"
	"bitbucket.org/daddysmurf/optanix-test/log"
	"encoding/json"
	"fmt"
	"net/http"
)

const minimumSentenceCount = 2

type ExpectedPayload struct {
	Sentences string `json:"sentences"`
}

type ResponsePayload struct {
	Valid  bool           `json:"valid"`
	Error  string         `json:"error"`
	Result map[string]int `json:"result"`
}

func logRequest(request *http.Request) {
	// <client> <method> <path>
	log.Info(fmt.Sprintf("%s %s %s", request.RemoteAddr, request.Method, request.URL.Path))
	//fmt.Printf("%s %s %s", request.RemoteAddr, request.Method, request.URL.Path)
}

func demandPost(response http.ResponseWriter, request *http.Request) bool {
	if request.Method != "POST" {
		response.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(response, "This method only accepts POST requests.")
		return true
	}
	return false
}

func writeResponse(responsePayload ResponsePayload, response http.ResponseWriter, request *http.Request) {
	if responsePayload.Error != "" {
		responsePayload.Valid = false
		responsePayload.Result = nil
		log.Error(responsePayload.Error)
	}

	var bodyS string
	bodyBa, marshalErr := json.Marshal(responsePayload)
	if marshalErr != nil {
		bodyS = fmt.Sprintf("Error marshalling failure response: %s", marshalErr.Error())
		bodyBa = []byte(bodyS)
		log.Error(bodyS)
	} else {
		bodyS = string(bodyBa[:])
	}
	fmt.Fprintf(response, bodyS)
}

func wordCountPerSentence(request *http.Request) (responsePayload ResponsePayload) {
	responsePayload.Result = map[string]int{}

	payload := ExpectedPayload{}
	//var body []byte
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&payload)
	if err != nil {
		responsePayload.Error = fmt.Sprintf("Error reading and parsing body: %s", err.Error())
		return
	}

	responsePayload.Result, err = actualWork.CountWordsInSentences(payload.Sentences)
	if err != nil {
		responsePayload.Error = fmt.Sprintf("Error counting words in sentences: %s", err.Error())
		return
	}

	if len(responsePayload.Result) < minimumSentenceCount {
		responsePayload.Error = fmt.Sprintf("Not enough sentences. Found %d sentences, but at least %d are required.", len(responsePayload.Result), minimumSentenceCount)
		return
	}

	responsePayload.Valid = true
	return
}

func WordCountPerSentence(response http.ResponseWriter, request *http.Request) {
	/*
	 * word_count_per_sentence - Return individual sentences along with a word count for the sentence. For the purposes
	 * of this exercise, a sentence is any grouping of letters and words ending in a period, question mark or
	 * exclamation point. A word is text delimited by spaces.
	 */
	logRequest(request)
	if demandPost(response, request) {
		return
	}

	responsePayload := wordCountPerSentence(request)
	writeResponse(responsePayload, response, request)
}

func totalLetterCount(request *http.Request) (responsePayload ResponsePayload) {
	responsePayload.Result = map[string]int{}

	payload := ExpectedPayload{}
	//var body []byte
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&payload)
	if err != nil {
		responsePayload.Error = fmt.Sprintf("Error reading and parsing body: %s", err.Error())
		return
	}

	split, err := actualWork.SplitSentences(payload.Sentences)
	if err != nil {
		responsePayload.Error = fmt.Sprintf("Error splitting sentences: %s", err.Error())
		return
	}
	if len(split) < minimumSentenceCount {
		responsePayload.Error = fmt.Sprintf("Not enough sentences. Found %d sentences, but at least %d are required.", len(responsePayload.Result), minimumSentenceCount)
		return
	}

	responsePayload.Result, err = actualWork.CountLetters(payload.Sentences)
	if err != nil {
		responsePayload.Error = fmt.Sprintf("Error counting letters in sentences: %s", err.Error())
		return
	}

	responsePayload.Valid = true
	return
}

func TotalLetterCount(response http.ResponseWriter, request *http.Request) {
	/*
	 * total_letter_count - For each case-insensitive letter, return the number of times that letter appears in the
	 * entire text. E.g. the text contains 50 As, 40 Bs, etc.
	 */
	logRequest(request)
	if demandPost(response, request) {
		return
	}

	responsePayload := totalLetterCount(request)
	writeResponse(responsePayload, response, request)
}
